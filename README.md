Welcome to the cardboard battle project!
Using Spring, websockets, and nipplejs on a raspberry pi.

command to run on pi `sudo java -jar -Dspring.profiles.active=pi cardboard-battle-bot-1.0-SNAPSHOT.jar --server.port=80`

The pi profile wires the real gpio bean while another profile wires a fake.
