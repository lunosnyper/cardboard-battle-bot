package com.lunarwing.battlebot;

import com.lunarwing.battlebot.entity.Body;
import com.lunarwing.battlebot.entity.Message;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.lang.Nullable;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationTest {

    @LocalServerPort
    private int port;

    CompletableFuture<Body> completableFuture;

    @Test
    @DisplayName("Integration test loads")
    public void contextLoads() {
        assertTrue(true);
    }

    @Test
    @DisplayName("WebSockets are working")
    public void theWebSockets() {
        completableFuture = new CompletableFuture<>();

        StompSession stompSession = null;
        try {
            stompSession = getStompSession();
        } catch (Exception e) {
            e.printStackTrace();
        }

        stompSession.subscribe("/topic/status", new StompSessionHandlerAdapter() {
            @Override
            public Type getPayloadType(StompHeaders headers) {
                return Body.class;
            }

            @Override
            public void handleFrame(StompHeaders headers, @Nullable Object payload) {
                System.out.println(payload);
                completableFuture.complete((Body) payload);
            }
        });

        HashMap<String, Double> angle = new HashMap<String, Double>(){{
            put("degree", 123.0D);
        }};
        Body body = new Body(1, new HashMap<>(), 1.2D, 2.1D, 2.1D, angle);

        stompSession.send("/app/bot", body);

        Body actual = null;
        try {
            actual = completableFuture.get(10, SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assertions.assertThat(actual).isEqualTo(body);
    }

    StompSession getStompSession() throws InterruptedException, ExecutionException, TimeoutException {
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        return stompClient.connect("ws://localhost:" + port + "/battlebot", new StompSessionHandlerAdapter() {
        }).get(1,
                SECONDS);
    }

    private List<Transport> createTransportClient() {
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        return transports;
    }
}