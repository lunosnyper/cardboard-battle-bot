package com.lunarwing.battlebot.controller

import com.lunarwing.battlebot.entity.Body
import com.lunarwing.battlebot.entity.MotorState
import com.lunarwing.battlebot.service.GPIOService
import com.lunarwing.battlebot.service.MessageService
import spock.lang.Specification

class BotControllerTest extends Specification {

    MessageService messageService
    GPIOService gpioService
    BotController botController

    def setup() {
        messageService = Mock(MessageService.class)
        gpioService = Mock(GPIOService.class)
        botController = new BotController(messageService, gpioService)
    }

    def "parses the messages" () {
        when:
        Body body = botController.bot(new Body(identifier: 1,
                position: ["x":636.0D, "y": 423.0D],
                force: 123.1,
                pressure: 1234.4,
                distance: 123.1,
                angle: ["radian":3.49066D, "degree":200.0000857D]))

        then:
        1 * messageService.parse(_) >> new MotorState(1, 1, MotorState.FORWARD, 1)
        body.identifier == 1
    }

    def "sets the motor state" () {
        when:
        botController.bot(new Body(identifier: 1,
                position: ["x":636.0D, "y": 423.0D],
                force: 123.1,
                pressure: 1234.4,
                distance: 123.1,
                angle: ["radian":3.49066D, "degree":200.0000857D]))

        then:
        messageService.parse(_) >> new MotorState(1, 1, MotorState.FORWARD, 1)
        1 * gpioService.setMotorState(new MotorState(1, 1, MotorState.FORWARD, 1))
    }

}
