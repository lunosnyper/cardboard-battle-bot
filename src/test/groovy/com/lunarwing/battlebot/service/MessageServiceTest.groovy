package com.lunarwing.battlebot.service

import com.lunarwing.battlebot.entity.Body
import com.lunarwing.battlebot.entity.MotorState
import spock.lang.Specification
import spock.lang.Unroll

class MessageServiceTest extends Specification {

    MessageService messageService

    def setup() {
        messageService = new MessageService()
    }

    @Unroll
    def "pares the angle and force to the appropriate direction and speed"() {
        expect:
        def body = new Body(identifier: 1,
                position: ["x": 636.0D, "y": 423.0D],
                force: force,
                pressure: 1234.4,
                distance: 123.1,
                angle: ["radian": 4.712388997826D, "degree": (Double) degree])
        MotorState state = messageService.parse(body)
        state.leftMotor == leftMotor
        state.rightMotor == rightMotor

        where:
        leftMotor | rightMotor | force | degree
        500       | 0          | 3D    | 1.000D
        500       | 0          | 3D    | 10.000D
        500       | 1          | 3D    | 10.001D
        500       | 499        | 3D    | 80.000D
        500       | 500        | 3D    | 80.001D
        499       | 500        | 3D    | 100.001D
        1         | 500        | 3D    | 170.000D
        0         | 500        | 3D    | 170.001D
        500       | 0          | 3D    | 180.001D
        500       | 1          | 3D    | 190.001D
        500       | 499        | 3D    | 260.000D
        500       | 500        | 3D    | 260.001D
        499       | 500        | 3D    | 280.001D
        1         | 500        | 3D    | 350.000D
        0         | 500        | 3D    | 350.001D
        250       | 250        | 1.5D  | 90D
        246       | 250        | 1.5D  | 101.000D
    }

    def "turns back right when the degree is set in the 180-360 half"() {
        given:
        def body = new Body(identifier: 1,
                position: ["x": 636.0D, "y": 423.0D],
                force: 123.1,
                pressure: 1234.4,
                distance: 123.1,
                angle: ["radian": 4.712388997826D, "degree": 270.000000D])

        when:
        MotorState motorState = messageService.parse(body)

        then:
        motorState.half == MotorState.BACK
    }

    def "turns back left when the degree is set in the 0-180 quadrant"() {
        given:
        Body body = new Body(identifier: 1,
                position: ["x": 636.0D, "y": 423.0D],
                force: 123.1,
                pressure: 1234.4,
                distance: 123.1,
                angle: ["radian": 3.49066D, "degree": 90.0000857D])

        when:
        MotorState motorState = messageService.parse(body)

        then:
        motorState.half == MotorState.FORWARD
    }

    def "stops when the degree is null"() {

        given:
        Body body = new Body(identifier: 1,
                position: ["x": 636.0D, "y": 423.0D],
                force: null,
                pressure: null,
                distance: null,
                angle: null)

        when:
        MotorState motorState = messageService.parse(body)

        then:
        motorState.rightMotor == 0
        motorState.leftMotor == 0
        motorState.half == MotorState.STOP
    }

}
