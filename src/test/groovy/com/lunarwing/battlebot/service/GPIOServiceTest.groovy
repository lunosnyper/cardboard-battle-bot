package com.lunarwing.battlebot.service

import com.lunarwing.battlebot.entity.FakeGpioPinDigitalOutput
import com.lunarwing.battlebot.entity.FakeGpioPinPwmOutput
import com.lunarwing.battlebot.entity.MotorState
import com.pi4j.io.gpio.GpioPinDigitalOutput
import com.pi4j.io.gpio.GpioPinPwmOutput
import spock.lang.Specification

class GPIOServiceTest extends Specification {

    // pins on the l298n are labeled 1-4 on the board
    GpioPinDigitalOutput pin1In
    GpioPinDigitalOutput pin2In
    GpioPinDigitalOutput pin3In
    GpioPinDigitalOutput pin4In

    GpioPinPwmOutput pin1Pwm
    GpioPinPwmOutput pin2Pwm

    GPIOService gpioService

    def setup() {
        pin1In = new FakeGpioPinDigitalOutput()
        pin2In = new FakeGpioPinDigitalOutput()
        pin3In = new FakeGpioPinDigitalOutput()
        pin4In = new FakeGpioPinDigitalOutput()

        pin1Pwm = new FakeGpioPinPwmOutput()
        pin2Pwm = new FakeGpioPinPwmOutput()
        gpioService = new GPIOService(pin1In, pin2In, pin3In, pin4In, pin1Pwm, pin2Pwm)
    }

    def "sets the pins of the motor state forward" () {
        when:
        gpioService.setMotorState(new MotorState(200, 200, MotorState.FORWARD, 1))

        then:
        pin1In.isHigh()
        pin2In.isLow()
        pin3In.isHigh()
        pin4In.isLow()

        pin1Pwm.getPwm() == 200
        pin2Pwm.getPwm() == 200

    }

    def "sets the pins of the motor state backward" () {
        when:
        gpioService.setMotorState(new MotorState(200, 200, MotorState.BACK, 1))

        then:
        pin1In.isLow()
        pin2In.isHigh()
        pin3In.isLow()
        pin4In.isHigh()

        pin1Pwm.getPwm() == 200
        pin2Pwm.getPwm() == 200
    }

    def "sets the pins of the motor state stop" () {
        when:
        gpioService.setMotorState(new MotorState(0, 0, MotorState.STOP, 1))

        then:
        pin1In.isLow()
        pin2In.isLow()
        pin3In.isLow()
        pin4In.isLow()

        pin1Pwm.getPwm() == 0
        pin2Pwm.getPwm() == 0
    }

    def "turns the gpio on"() {
        when:
        gpioService.turnOn()

        then:
        pin1In.isHigh()
    }

    def "turns the gpio off"() {
        when:
        gpioService.turnOff()

        then:
        pin1In.isLow()
    }
}
