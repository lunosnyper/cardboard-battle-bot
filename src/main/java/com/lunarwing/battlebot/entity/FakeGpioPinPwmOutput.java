package com.lunarwing.battlebot.entity;

import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinListener;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class FakeGpioPinPwmOutput implements GpioPinPwmOutput {
    private int value;

    @Override
    public void setPwm(int value) {
        this.value = value;
    }

    @Override
    public void setPwmRange(int range) {

    }

    @Override
    public int getPwm() {
        return value;
    }

    @Override
    public GpioProvider getProvider() {
        return null;
    }

    @Override
    public Pin getPin() {
        return null;
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setTag(Object tag) {

    }

    @Override
    public Object getTag() {
        return null;
    }

    @Override
    public void setProperty(String key, String value) {

    }

    @Override
    public boolean hasProperty(String key) {
        return false;
    }

    @Override
    public String getProperty(String key) {
        return null;
    }

    @Override
    public String getProperty(String key, String defaultValue) {
        return null;
    }

    @Override
    public Map<String, String> getProperties() {
        return null;
    }

    @Override
    public void removeProperty(String key) {

    }

    @Override
    public void clearProperties() {

    }

    @Override
    public void export(PinMode mode) {

    }

    @Override
    public void export(PinMode mode, PinState defaultState) {

    }

    @Override
    public void unexport() {

    }

    @Override
    public boolean isExported() {
        return false;
    }

    @Override
    public void setMode(PinMode mode) {

    }

    @Override
    public PinMode getMode() {
        return null;
    }

    @Override
    public boolean isMode(PinMode mode) {
        return false;
    }

    @Override
    public void setPullResistance(PinPullResistance resistance) {

    }

    @Override
    public PinPullResistance getPullResistance() {
        return null;
    }

    @Override
    public boolean isPullResistance(PinPullResistance resistance) {
        return false;
    }

    @Override
    public Collection<GpioPinListener> getListeners() {
        return null;
    }

    @Override
    public void addListener(GpioPinListener... listener) {

    }

    @Override
    public void addListener(List<? extends GpioPinListener> listeners) {

    }

    @Override
    public boolean hasListener(GpioPinListener... listener) {
        return false;
    }

    @Override
    public void removeListener(GpioPinListener... listener) {

    }

    @Override
    public void removeListener(List<? extends GpioPinListener> listeners) {

    }

    @Override
    public void removeAllListeners() {

    }

    @Override
    public GpioPinShutdown getShutdownOptions() {
        return null;
    }

    @Override
    public void setShutdownOptions(GpioPinShutdown options) {

    }

    @Override
    public void setShutdownOptions(Boolean unexport) {

    }

    @Override
    public void setShutdownOptions(Boolean unexport, PinState state) {

    }

    @Override
    public void setShutdownOptions(Boolean unexport, PinState state, PinPullResistance resistance) {

    }

    @Override
    public void setShutdownOptions(Boolean unexport, PinState state, PinPullResistance resistance, PinMode mode) {

    }
}
