package com.lunarwing.battlebot.entity;

import java.util.Map;

public class Body {
    int identifier;
    Map<String, Double> position;
    Double force;
    Double pressure;
    Double distance;
    Map<String,Double> angle;

    public Body() {}
    public Body(int identifier,
            Map<String, Double> position,
            Double force,
            Double pressure,
            Double distance,
            Map<String,Double> angle) {
        this.identifier = identifier;
        this.position = position;
        this.force = force;
        this.pressure = pressure;
        this.distance = distance;
        this.angle = angle;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public Map<String, Double> getPosition() {
        return position;
    }

    public void setPosition(Map<String, Double> position) {
        this.position = position;
    }

    public Double getForce() {
        return force;
    }

    public void setForce(Double force) {
        this.force = force;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Map<String, Double> getAngle() {
        return angle;
    }

    public void setAngle(Map<String, Double> angle) {
        this.angle = angle;
    }

    @Override
    public String toString() {
        return "Body{" +
                "identifier=" + identifier +
                ", position=" + position +
                ", force=" + force +
                ", pressure=" + pressure +
                ", distance=" + distance +
                ", angle=" + angle +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Body body = (Body) o;

        if (identifier != body.identifier) return false;
        if (position != null ? !position.equals(body.position) : body.position != null) return false;
        if (force != null ? !force.equals(body.force) : body.force != null) return false;
        if (pressure != null ? !pressure.equals(body.pressure) : body.pressure != null) return false;
        if (distance != null ? !distance.equals(body.distance) : body.distance != null) return false;
        return angle != null ? angle.equals(body.angle) : body.angle == null;
    }

    @Override
    public int hashCode() {
        int result = identifier;
        result = 31 * result + (position != null ? position.hashCode() : 0);
        result = 31 * result + (force != null ? force.hashCode() : 0);
        result = 31 * result + (pressure != null ? pressure.hashCode() : 0);
        result = 31 * result + (distance != null ? distance.hashCode() : 0);
        result = 31 * result + (angle != null ? angle.hashCode() : 0);
        return result;
    }
}
