package com.lunarwing.battlebot.entity;

public class MotorState {
    public static int BACK = 2;
    public static int FORWARD = 1;
    public static int STOP = 0;

    public int leftMotor;
    public int rightMotor;
    public int half;

    public MotorState(int leftMotor, int rightMotor, int half, double currentSpeedRatio) {
        if (currentSpeedRatio < 1) {
            leftMotor = (int) Math.round(leftMotor * currentSpeedRatio);
            rightMotor = (int) Math.round(rightMotor * currentSpeedRatio);
        }
        this.leftMotor = leftMotor;
        this.rightMotor = rightMotor;
        this.half = half;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MotorState that = (MotorState) o;

        if (leftMotor != that.leftMotor) return false;
        if (rightMotor != that.rightMotor) return false;
        return half == that.half;
    }

    @Override
    public int hashCode() {
        int result = leftMotor;
        result = 31 * result + rightMotor;
        result = 31 * result + half;
        return result;
    }
}
