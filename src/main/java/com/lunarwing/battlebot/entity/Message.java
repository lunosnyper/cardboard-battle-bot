package com.lunarwing.battlebot.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Message {

    public Body body;

    public Message() {}
    public Message(Body body) {this.body = body;}

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return "{ \"body\": " + objectMapper.writeValueAsString(this.body) + "}";
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }
}
