package com.lunarwing.battlebot.service;

import com.lunarwing.battlebot.entity.MotorState;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinPwmOutput;

public class GPIOService {

    private final GpioPinDigitalOutput pin1In;
    private final GpioPinDigitalOutput pin2In;
    private final GpioPinDigitalOutput pin4In;
    private final GpioPinDigitalOutput pin3In;
    private final GpioPinPwmOutput pinPwmOutput1;
    private final GpioPinPwmOutput pinPwmOutput2;

    public GPIOService(GpioPinDigitalOutput pin1In,
                       GpioPinDigitalOutput pin2In,
                       GpioPinDigitalOutput pin3In,
                       GpioPinDigitalOutput pin4In,
                       GpioPinPwmOutput pinPwmOutput1,
                       GpioPinPwmOutput pinPwmOutput2
                       ) {
        this.pin1In = pin1In;
        this.pin2In = pin2In;
        this.pin3In = pin3In;
        this.pin4In = pin4In;
        this.pinPwmOutput1 = pinPwmOutput1;
        this.pinPwmOutput2 = pinPwmOutput2;
    }

    public void turnOn() {
        pin1In.high();
    }

    public void turnOff() {
        pin1In.low();
    }

    public void forward() {
        pin1In.high();
        pin2In.low();
        pin3In.high();
        pin4In.low();
    }

    public void backward() {
        pin1In.low();
        pin2In.high();
        pin3In.low();
        pin4In.high();
    }

    public void stop() {
        pin1In.low();
        pin2In.low();
        pin3In.low();
        pin4In.low();
    }

    public void setLeftMotor(int speed) {
        pinPwmOutput2.setPwm(speed);
    }

    public void setRightMotor(int speed) {
        pinPwmOutput1.setPwm(speed);
    }

    public void setMotorState(MotorState motorState) {
        if(motorState.half == MotorState.FORWARD)
            forward();
        else if(motorState.half == MotorState.BACK)
            backward();
        else
            stop();

        setLeftMotor(motorState.leftMotor);
        setRightMotor(motorState.rightMotor);
    }
}
