package com.lunarwing.battlebot.service;

import com.lunarwing.battlebot.entity.Body;
import com.lunarwing.battlebot.entity.MotorState;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    private int MAX_SPEED = 500;
    private int ONE_LESS_THAN_MAX = MAX_SPEED - 1;

    public MessageService() {

    }

    public MotorState parse(Body body) {

        if (body.getAngle() == null) {
            return new MotorState(0, 0, MotorState.STOP, 1);
        }

        Double degree = body.getAngle().get("degree");

        int rightMotorSpeed;
        int leftMotorSpeed;
        int quadrant;

        double currentSpeedRatio = (body.getForce() / 3D);

        if (degree > 270.00000) {
            quadrant = MotorState.BACK;
            rightMotorSpeed = MAX_SPEED;
            leftMotorSpeed = MAX_SPEED - getMotorSpeed(degree - 270);
        } else if (degree > 180.000) {
            quadrant = MotorState.BACK;
            rightMotorSpeed = getMotorSpeed(degree - 180);
            leftMotorSpeed = MAX_SPEED;
        } else if (degree > 90.000) {
            quadrant = MotorState.FORWARD;
            rightMotorSpeed = MAX_SPEED;
            leftMotorSpeed = MAX_SPEED - getMotorSpeed(degree - 90);
        } else {
            quadrant = MotorState.FORWARD;
            rightMotorSpeed = getMotorSpeed(degree);
            leftMotorSpeed = MAX_SPEED;
        }


        return new MotorState(leftMotorSpeed, rightMotorSpeed, quadrant, currentSpeedRatio);
    }

    private int getMotorSpeed(Double degree) {
        int motorSpeed = 0;
        if (degree < 10) {

        } else if (degree > 80) {
            motorSpeed = MAX_SPEED;
        } else {
            motorSpeed = (int) Math.ceil((degree - 10) / 70 * ONE_LESS_THAN_MAX);
        }
        return motorSpeed;
    }
}
