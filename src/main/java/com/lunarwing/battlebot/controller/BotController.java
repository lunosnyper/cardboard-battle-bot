package com.lunarwing.battlebot.controller;

import com.lunarwing.battlebot.entity.Body;
import com.lunarwing.battlebot.entity.MotorState;
import com.lunarwing.battlebot.service.GPIOService;
import com.lunarwing.battlebot.service.MessageService;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class BotController {

    private final MessageService messageService;
    private final GPIOService gpioService;

    public BotController(MessageService messageService, GPIOService gpioService) {
        this.messageService = messageService;
        this.gpioService = gpioService;
    }

    @MessageMapping("/bot")
    @SendTo("/topic/status")
    public Body bot(Body body){
        System.out.println("this is in the code : " + body);
        MotorState motorState = messageService.parse(body);
        gpioService.setMotorState(motorState);
        return body;
    }
}
