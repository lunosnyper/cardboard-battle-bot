package com.lunarwing.battlebot;

import com.lunarwing.battlebot.entity.FakeGpioPinDigitalOutput;
import com.lunarwing.battlebot.entity.FakeGpioPinPwmOutput;
import com.lunarwing.battlebot.service.GPIOService;
import com.pi4j.io.gpio.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/battlebot").withSockJS();
    }

    @Bean
    @Profile("pi")
    public GPIOService getBlinkyPin() {
        GpioController gpioController = GpioFactory.getInstance();
        GpioPinDigitalOutput gpioPin1 = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_15, "in1", PinState.LOW);
        GpioPinDigitalOutput gpioPin2 = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_16, "in2", PinState.LOW);
        GpioPinDigitalOutput gpioPin3 = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_05, "in3", PinState.LOW);
        GpioPinDigitalOutput gpioPin4 = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_04, "in4", PinState.LOW);

        GpioPinPwmOutput gpioPwm1 = gpioController.provisionPwmOutputPin(RaspiPin.GPIO_01, "pwmRight");
        GpioPinPwmOutput gpioPwm2 = gpioController.provisionPwmOutputPin(RaspiPin.GPIO_26, "pwmLeft");
        return new GPIOService(gpioPin1, gpioPin2, gpioPin3, gpioPin4, gpioPwm1, gpioPwm2);
    }

    @Bean
    @Profile("!pi")
    public GPIOService getFakeBlinkyPin() {
        GpioPinDigitalOutput gpioPin1 = new FakeGpioPinDigitalOutput();
        GpioPinDigitalOutput gpioPin2 = new FakeGpioPinDigitalOutput();
        GpioPinDigitalOutput gpioPin3 = new FakeGpioPinDigitalOutput();
        GpioPinDigitalOutput gpioPin4 = new FakeGpioPinDigitalOutput();

        GpioPinPwmOutput gpioPwm1 = new FakeGpioPinPwmOutput();
        GpioPinPwmOutput gpioPwm2 = new FakeGpioPinPwmOutput();
        return new GPIOService(gpioPin1, gpioPin2, gpioPin3, gpioPin4, gpioPwm1, gpioPwm2);
    }

}
