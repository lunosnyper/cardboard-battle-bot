var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    } else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/battlebot');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/status', function (greeting) {
            showGreeting(JSON.parse(greeting.body));
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    stompClient.send("/app/bot", {}, JSON.stringify({'body': $("#name").val()}));
}

let inThrottle = false;

function sendData(data) {
    if (data.force !== "null") {
        if (!inThrottle) {
            stompClient.send("/app/bot", {}, JSON.stringify(data));
            inThrottle = true;
            setTimeout(() => inThrottle = false, 80);
        }
        return;
    }
    stompClient.send("/app/bot", {}, JSON.stringify(data));
}

function showGreeting(message) {
    $("#greetings").html("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $("#connect").click(function () {
        connect();
    });
    $("#disconnect").click(function () {
        disconnect();
    });
    $("#send").click(function () {
        sendName();
    });
});